const express = require('express')
//connection file
require('./db/mongoose')

//models
const User = require('./models/user')
const Task = require('./models/task')

//router 
const userRouter = require('./routers/user')
const taskRouter = require('./routers/task')

const app = express()
// const port = process.env.PORT || 3000
const port = process.env.PORT

//disable get methods
// app.use((req, res, next) => {
//     if (req.method == 'GET') {
//         res.send('GET requests are disabled')
//     }else{
//         next()
//     }
// })

//under maintanance code
// app.use((req, res, next) => {
//     res.status(503).send('Site is currently down. Check back soon!')
// })


app.use(express.json())
app.use(userRouter) 
app.use(taskRouter) 

app.listen(port, () => {
    console.log('server is up on port' + port)
})
