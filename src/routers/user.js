const express = require('express')
const multer = require('multer')
const sharp = require('sharp')
const User = require('../models/user')
const auth = require('../middleware/auth')
const router = new express.Router()

router.post('/users', async (req, res) => {
    
    const user = new User(req.body);

    try {
        await user.save()
        const token = await user.genrateAuthToken() 
        res.status(201).send({ user, token })
    } catch (e) {
        res.status(400).send(e)
    }

})

router.post('/user/login', async (req, res) => {

    try {
            
        const user = await User.findByCredentials(req.body.email, req.body.password)
        const token = await user.genrateAuthToken() 

        res.send({ user , token })

    } catch (e) {
            
        res.status(400).send(e)

    }

})

router.post('/users/logout', auth, async (req, res) => {
    
    try {
        req.user.tokens = req.user.tokens.filter((token) => {
            return token.token !== req.token
        })    
        await req.user.save()

        res.send()

    } catch (e) {
        res.send(500).send()
    }

})

//logout from all session
router.post('users/logoutAll', auth, async (req, res) => {

    try {
        
        req.user.token = []
        await req.user.save()
        res.send()
        
    } catch (e) {
        res.status(500).send()
    }

})



//get all users
router.get('/users/me', auth , async (req, res) => { 
    // User.find({ "name" : "Hello world"})
    res.send(req.user)
    
})

// router.get('/users/:id', async (req, res) => {
    
//     const _id = req.params.id
        
//     try {

//         const user = await User.findById(_id)

//         if(!user) {
//             return res.status(404).send()
//         }

//         res.send(user)

//     } catch (e) {
//         res.status(500).send()
//     }
    
// })


router.patch('/update-user/profile', auth, async (req, res) => {

    const updates = Object.keys(req.body)
    const allowedUpdates = ['name', 'email', 'password', 'age']
    const isValidOperation = updates.every((update) => allowedUpdates.includes(update) )

    if (!isValidOperation){
        return res.status(400).send({ error : 'Invalid updates!' })
    }

    try {

        // const user = await User.findById(req.params.id)
        updates.forEach((update) => req.user[update] = req.body[update])

        await req.user.save()

        // const user = await User.findByIdAndUpdate(req.params.id,req.body, { new: true, runValidators: true })    

        res.send(req.user)

    } catch (e) {
        res.status(400).send(e)
    }
})

router.delete('/remove-user/me', auth, async (req, res) => {

    try {
        // const user = await User.findByIdAndDelete(req.user._id)    

        // if(!user){
        //     return res.status(404).send()
        // }

        //short method
        await req.user.remove()
        res.send(req.user)

    } catch (e) {
        res.status(500).send(e)
    }

})


const upload = multer({
    limits: {
        fileSize: 1000000
    },
    fileFilter(req, file, cb) {
        // !file.originalname.endsWith('.jpg')
        if (!file.originalname.match(/\.(jpg|jpeg|png)$/)){
            return cb(new Error('Please upload valid image!'))
        }
        
        cb(undefined, true)
        // cb(undefined, false)
    }
})

router.post('/user/upload-profile', auth, upload.single('avatar'), async (req, res) => {

    const buffer = await sharp(req.file.buffer).resize({ width: 250, height: 250 }).png().toBuffer()
    //req.user.avatar = req.file.buffer
    req.user.avatar = buffer
    await req.user.save()
    res.send()
    
},(error, req, res, next) => {
    res.status(400).send({ error: error.message })
})

router.delete('/user/remove-profile', auth, async (req, res) => {

    req.user.avatar = undefined
    await req.user.save()
    res.send()
    
},(error, req, res, next) => {
    res.status(400).send({ error: error.message })
})

//get user avatar
router.get('/user/:id/avatar', async (req,res) => {
    console.log('here');
    try {
        const user = await User.findById(req.params.id)
        console.log(user)
        if (!user || !user.avatar) {
            throw new Error()
        }

        res.set('Content-type', 'image/png')
        res.send(user.avatar)

    } catch (e) {
        res.status(404).send()
    }

})

module.exports = router