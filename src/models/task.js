const mongoose = require('mongoose')
const validator = require('validator')

const taskSchema = new mongoose.Schema({
    description: {
        type: String,
        require: true,
    },
    completed: {
        type: Boolean,
        default: false,
    },
    owner: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'User'
    }
},{
    timestamps: true
})

// const task = new Task({
//     description: 'This is my description of task',
// })

// task.save().then(() => {
//     console.log(task)
// }).catch((error) => {
//     console.log('Error!', error)
// })

// const Task = mongoose.model('Task', {
//     description : {
//         type: String
//     }, completed : {
//         type: Boolean
//     }
// })

// const taskDetail = new Task({
//     description: 'This is new task',
//     completed: 'false'
// })

// taskDetail.save().then(() => {
//     console.log(taskDetail)
// }).catch((error) => {
//     console.log('Error!', error)
// })

const Task = mongoose.model('Task', taskSchema)
module.exports = Task