//CRUD
// const mongodb = require('mongodb')
// const MongoClient = mongodb.MongoClient
// const ObjectID = mongodb.ObjectID

const { MongoClient, ObjectID } = require('mongodb')

const connectionURL = 'mongodb://127.0.0.1:27017'
const databaseName = 'task-manager-app'
// const id = new ObjectID()
// console.log(id)
// console.log(id.getTimestamp())

MongoClient.connect(connectionURL, { useNewUrlParser: true }, (error, client) => {
    if(error){
        return console.log('Unable to connnect to database')
    }

    console.log('connected correctly!')

    const db = client.db(databaseName)

    //search by id
    // db.collection('users').findOne({ _id: new ObjectID("60493c9f48371e68a64e8876") }, (error,user) => {
    //     if(error){
    //         return console.log('Unable to fetch!')
    //     }
    //     console.log(user)
    // })

    // db.collection('users').find({ age: 27 }).toArray((error,user) => {
        
    //     console.log(user)
    // })

    //get task by id
    // db.collection('tasks').findOne({ _id : new ObjectID("604fa2243c4d703d1748a903")}, (error,task) => {
    //     console.log(task)
    // })

    //find task which is not completed
    // db.collection('tasks').find({ completed: false }).toArray((error,user) => {
    //     console.log(task)
    // })

    //search by name
    // db.collection('users').findOne({ name: 'Jen' }, (error,user) => {
    //     if(error){
    //         return console.log('Unable to fetch!')
    //     }
    //     console.log(user)
    // })

    // db.collection('users').insertOne({
    //     // _id : id, //we can use id
    //     name : 'Hello',
    //     age : 27
    // }, (error, result) => {
    //     if(error){
    //         return console.log('Unable to insert user!')
    //     }

    //     console.log(result.ops)
    // })

    // db.collection('users').insertMany([
    //     {
    //         name : 'Jen',
    //         age : 28
    //     }, {
    //         name : 'Gulter',
    //         age : 27
    //     }        
    // ],(error, result) => {
    //     if(error) {
    //         return console.log('Unable to insert document')
    //     }

    //     console.log(result.ops)
    // })

    // db.collection('tasks').insertMany([
    //     {
    //         description : 'test1',
    //         completed : false
    //     }, {
    //         description : 'test2',
    //         completed : true
    //     }, {
    //         description : 'test3',
    //         completed : false
    //     }       
    // ],(error, result) => {
    //     if(error) {
    //         return console.log('Unable to insert document')
    //     }

    //     console.log(result.ops)
    // })

    //update 
    // const updatePromise = db.collection('users').updateOne({
    //     _id: new ObjectID("60493ee595ca48696e150451")
    // }, {
    //     $set: {
    //         name : 'Mike'
    //     }
    // })

    // updatePromise.then((result) => {
    //     console.log(result)
    // }).catch((error) => {
    //     console.log(error)
    // })

    // db.collection('users').updateOne({
    //     _id: new ObjectID("60493ee595ca48696e150451")
    // }, {
    //     $set: {
    //         name : 'Mike'
    //     }
    // }).then((result) => {
    //     console.log(result)
    // }).catch((error) => {
    //     console.log(error)
    // })

    // db.collection('tasks').updateMany({
    //     completed: false
    // }, {
    //     $set: {
    //         completed : true
    //     }
    // }).then((result) => {
    //     console.log(result.modifiedCount)
    // }).catch((error) => {
    //     console.log(error)
    // })

    //delete
    db.collection('users').deleteMany({
        age: 27
    }).then((result) => {
        console.log(result)
    }).catch((error) => {
        console.log(error)
    })

    //delete one
    db.collection('tasks').deleteOne({
        description: 'test1'
    }).then((result) => {
        console.log(result)
    }).catch((error) => {
        console.log(error)
    })
})

