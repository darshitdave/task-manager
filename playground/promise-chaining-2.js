require('../src/db/mongoose')
const { findOneAndDelete, findByIdAndDelete } = require('../src/models/task')
const Task = require('../src/models/task')

// Task.findByIdAndDelete('5fe865e22bb2d14a0d4b8857').then((task) => {
//     console.log(task)

//     return Task.countDocuments({ completed : 'false' })
// }).then((result) => {
//     console.log(result)
// }).catch((e) => {
//     console.log(e)
// })

const deleteTaskAndCount = async (id,status) => {
    const task = await Task.findByIdAndDelete(id)
    const count = await Task.countDocuments({ completed : status })

    return count
}

deleteTaskAndCount('5fe78c139b9d921b92501d21', 'false').then((count) => {
    console.log(count)
}).catch((e) => {
    console.log(e)
})